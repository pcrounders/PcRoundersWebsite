<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.custom.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/full-slider.css" rel="stylesheet">
<style>
    /*-------------------------------*/
/*      Code snippet by          */
/*      @maridlcrmn              */
/*-------------------------------*/


section {
    padding-top: 100px;
    padding-bottom: 100px;
}

.quote {
    color: rgba(0,0,0,.1);
    text-align: center;
    margin-bottom: 30px;
}

/*-------------------------------*/
/*    Carousel Fade Transition   */
/*-------------------------------*/

#fade-quote-carousel.carousel {
  padding-bottom: 60px;
}
#fade-quote-carousel.carousel .carousel-inner .item {
  opacity: 0;
  -webkit-transition-property: opacity;
      -ms-transition-property: opacity;
          transition-property: opacity;
}
#fade-quote-carousel.carousel .carousel-inner .active {
  opacity: 1;
  -webkit-transition-property: opacity;
      -ms-transition-property: opacity;
          transition-property: opacity;
}
#fade-quote-carousel.carousel .carousel-indicators {
  bottom: 10px;
}
#fade-quote-carousel.carousel .carousel-indicators > li {
  background-color: #e84a64;
  border: none;
}
#fade-quote-carousel blockquote {
    text-align: center;
    border: none;
}
#fade-quote-carousel .profile-circle {
    width: 100px;
    height: 100px;
    margin: 0 auto;
    border-radius: 100px;
}
 * {
    box-sizing: border-box;
}

body {
    margin: 0;
}

.testnavbar {
    overflow: hidden;
   
    font-family: Arial, Helvetica, sans-serif;
}

.testnavbar a {
    float: left;
    font-size: 16px;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

.testdropdown {
    float: left;
    overflow: hidden;
}

.testdropdown .testdropbtn {
    font-size: 16px;    
    border: none;
    outline: none;
    color: white;
    padding: 14px 16px;
    background-color: inherit;
    font: inherit;
    margin: 0;
}

 .testdropdown:hover {
    background-color: red;
}

.testdropdown-content {
    display: none;
    position: absolute;
    background-color: white;
    width: 100%;
    left: 0;
    margin-top: 40px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.testdropdown-content .header {
    background: red;
    padding: 16px;
    color: white;
}

.testdropdown:hover .testdropdown-content {
    display: block;
}

/* Create three equal columns that floats next to each other */
.testcolumn {
    float: left;
    width: 15.33%;
    padding-left:90px;
    background-color: white;
    height:127px;
   
}

.testcolumn a {
    float: none;
    color: black;
    padding: 1px;
    text-decoration: none;
    display: block;
    text-align: left;
}


/* Clear floats after the columns */
.testrow:after {
    content: "";
    display: table;
    clear: both;
}

/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
    .testcolumn {
        width: 100%;
        height: auto;
    }
}
  body{
    width: 100%;
  }
  .footer {
   width: 100%;
   height: 50px;
   background-color: red;
   color: white;
   text-align: center;
}

li a:hover, .dropdown:hover {
    color:white;
}
 ul li:hover{
    background-color: red;
    color:white;
}
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}
.nav-link{
  color: white;
}
.footer {
   width: 100%;
   height: 50px;
   background-color:rgb(100, 89, 89);
   color: white;
   text-align: center;
}

.dropdown-content a:hover { background-color:rgb(207, 20, 60);}

.dropdown:hover .dropdown-content {
    display: block;
}
.centered {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}
.navbar-brand {
  padding: 0px;
}
.navbar-brand>img {
  height: 52px;
  padding: 0px;
  width: 100px;
}

  </style>
  <script>
 

</script>
  </head>  

  <body>
    
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-expand-lg navbar-dark bg-dark fixed-top" style="position: fixed;">
     
          <a class="navbar-brand" href="index.html" ><img class="img-responsive" src="images/img3.jpeg" width="50px;" height="50px;"></a> 
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto pull-right" style="color:white;">
            <li class="nav-item" >
              <a class="nav-link" href="index.html" style="color:white;">Home&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
              </a>
            </li>
            <li class="nav-item hidden-sm hidden-md hidden-xs">
              <a class="nav-link" href="#aboutus"  style="color:white;">About&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
            </li>
            <li class="nav-item hidden-lg">
              <a class="nav-link" href="Technology.html"  style="color:white;">Technology&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
            </li>
            <li class="nav-item hidden-lg">
                <a class="nav-link" href="Services.html"  style="color:white;">Services&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
              </li>
              <li class="nav-item hidden-lg">
                  <a class="nav-link" href="VirtualRepair.html"  style="color:white;">Virtual Repair&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                </li>
            <li class="nav-item testnavbar hidden-sm hidden-md hidden-xs" style="padding-right:10px;">
              <div class="testdropdown ">
                  <a class="testdropbtn">Technology 
                  </a>
                  <div class="testdropdown-content">
                      <center><h1 style="color:red;font-family: Arial !important;font-size: 40px !important;">Technology</h1></center>
                      <hr style="border-top:1px dotted rgb(146, 11, 29);"/>
                      <div class="testrow">
                        
                       <div class="testcolumn">
                       <a href="#" style="text-decoration:none;"><img src="images/ASP.png"></a>
                       <h4 style="color:black;padding-left:5px;">Asp.net</h4>          
                      </div>
                       <div class="testcolumn">
                         <a href="#"><img src="images/html5.png"></a>
                          <h4 style="color:black;padding-left:5px;">Html5</h4>       
                       </div> 
                       <div class="testcolumn">
                          <a href="#"><img src="images/v-1.png"></a>
                          <h4 style="color:black;padding-left:5px;">Vue.js</h4>          
                       </div> 
                      <div class="testcolumn">
                          <a href="#"><img src="images/MEAN.png"></a>
                          <h4 style="color:black;padding-left:5px;">MEAN</h4>           
                      </div>
                        <div class="testcolumn">
                            <a href="#"><img src="images/xamarin-1.png"></a>
                            <h4 style="color:black;padding-left:5px;">Xamarin / PhoneGap</h4>          
                       </div>
                         <div class="testcolumn">
                            <a href="#"><img src="images/wordPress-1.png"></a>
                            <h4 style="color:black;padding-left:5px;">WordPress</h4>         
                        </div> 
                      
                             
                             <div class="testcolumn">
                              <a href="#"><img src="images/wpf.png"></a>
                              <h4 style="color:black;padding-left:5px;">WPF / MVVM</h4>          
                             </div>
                              <div class="testcolumn">
                                <a href="#"><img src="images/bootstrap.png"></a>
                                 <h4 style="color:black;padding-left:5px;">Twitter Bootstrap</h4>       
                              </div> 
                              <div class="testcolumn">
                                 <a href="#"><img src="images/ember-1.png"></a>
                                 <h4 style="color:black;padding-left:5px;">Ember JS</h4>          
                              </div> 
                             <div class="testcolumn">
                                 <a href="#"><img src="images/ios-1.png"></a>
                                 <h4 style="color:black;padding-left:5px;">iOS</h4>           
                             </div>
                               <div class="testcolumn">
                                   <a href="#"><img src="images/raspberry.png"></a>
                                   <h4 style="color:black;padding-left:5px;">Raspberry Pi</h4>          
                              </div>
                                <div class="testcolumn">
                                   <a href="#"><img src="images/magento.png"></a>
                                   <h4 style="color:black;padding-left:5px;">Magento</h4>         
                               </div> 
                             
                               <div class="testcolumn">
                                <a href="#"><img src="images/azure-1.png"></a>
                                <h4 style="color:black;padding-left:5px;">Windows Azure</h4>          
                               </div>
                                <div class="testcolumn">
                                  <a href="#"><img src="images/angular-1.png"></a>
                                   <h4 style="color:black;padding-left:5px;">Angular JS</h4>       
                                </div> 
                                <div class="testcolumn">
                                   <a href="#"><img src="images/react-1.png"></a>
                                   <h4 style="color:black;padding-left:5px;">React.JS</h4>          
                                </div> 
                               <div class="testcolumn">
                                   <a href="#"><img src="images/android-1.png"></a>
                                   <h4 style="color:black;padding-left:5px;">Android</h4>           
                               </div>
                                 <div class="testcolumn">
                                     <a href="#"><img src="images/php.png"></a>
                                     <h4 style="color:black;padding-left:5px;">PHP</h4>          
                                </div>
                                  <div class="testcolumn">
                                     <a href="#"><img src="images/joomla.png"></a>
                                     <h4 style="color:black;padding-left:5px;">Joomla</h4>         
                                 </div> 
                             
                                 <div class="testcolumn">
                                  <a href="#"><img src="images/rail.png"></a>
                                  <h4 style="color:black;padding-left:5px;">Ruby on Rails</h4>          
                                 </div>
                                  <div class="testcolumn">
                                    <a href="#"><img src="images/backbone.png"></a>
                                     <h4 style="color:black;padding-left:5px;">Backbone Js</h4>       
                                  </div> 
                                  <div class="testcolumn">
                                     <a href="#"><img src="images/drupal.png"></a>
                                     <h4 style="color:black;padding-left:5px;">Drupal</h4>          
                                  </div> 
                                 <div class="testcolumn">
                                     <a href="#"><img src="images/laravel.png"></a>
                                     <h4 style="color:black;padding-left:5px;">Laravel</h4>           
                                 </div>
                                   <div class="testcolumn">
                                       <a href="#"><img src="images/codeigniter.png"></a>
                                       <h4 style="color:black;padding-left:5px;">Codeigniter</h4>          
                                  </div>
                                    <div class="testcolumn">
                                       <a href="#"><img src="images/node.png"></a>
                                       <h4 style="color:black;padding-left:5px;">Node JS</h4>         
                                   </div> 
                          </div>

                    </div>
                    </div> 
              
                
            </li>
            <li class="nav-item testnavbar hidden-sm hidden-md hidden-xs" style="padding-right:10px;">
              <div class="testdropdown">
                  <a class="testdropbtn">Services 
                  </a>
                  <div class="testdropdown-content" style="height:500px;">
                      <center><h1 style="color:red;font-family: Arial !important;font-size: 40px !important;">Services</h1></center>
                      <hr style="border-top:1px dotted rgb(146, 11, 29);"/>
                      <div class="testrow">
                        
                       <div class="testcolumn" style="width:24%;margin-left:220px;">
                       <h3 style="font-size: 18px; color: #ca0000;font-family: FontAwesome;font-weight: normal;">HIRE & OFFSHORE SERVICES</h3>
                        <a href="#" style="color: #ca6666;text-decoration:none;">Offshore Development Services</a>
                        <a href="#" style="color: #ca6666;text-decoration:none;">Hire dedicated Developers</a>
                        <a href="#" style="color: #ca6666;text-decoration:none;">QA/Testing Services</a>
                        <a href="#" style="color: #ca6666;text-decoration:none;">IT Consulting</a>

                      </div>
                      <div class="testcolumn" style="width:24%;">
                        <h3 style="font-size: 18px; color: #ca0000;font-family: FontAwesome;font-weight: normal;">FRONTEND DEVELOPMENT</h3>
                         <a href="#" style="color: #ca6666;text-decoration:none;">Angular JS Development</a>
                         <a href="#" style="color: #ca6666;text-decoration:none;">Backbone JS Development</a>
                         <a href="#" style="color: #ca6666;text-decoration:none;">Node JS Development</a>
                         <a href="#" style="color: #ca6666;text-decoration:none;">Bootstrap Development</a>

                       </div>
                       <div class="testcolumn" style="width:24%;">
                        <h3 style="font-size: 18px; color: #ca0000;font-family: FontAwesome;font-weight: normal;">SOFTWARE & WEB DEVELOPMENT</h3>
                         <a href="#" style="color: #ca6666;text-decoration:none;">Asp.Net / MVC Development</a>
                         <a href="#" style="color: #ca6666;text-decoration:none;">C# / WPF Development</a>
                         <a href="#" style="color: #ca6666;text-decoration:none;">Windows Azure Development</a>
                         <a href="#" style="color: #ca6666;text-decoration:none;">Java/J2ee Development</a>
                         <a href="#" style="color: #ca6666;text-decoration:none;">PHP Web Development</a>
                       </div>
                      </div>
                      <div class="testrow" style="margin-top:70px;">                          
                       <div class="testcolumn" style="width:24%;margin-left:220px;" style="padding-top:60px;">
                       <h3 style="font-size: 18px; color: #ca0000;font-family: FontAwesome;font-weight: normal;">MOBILE APP DEVELOPMENT</h3>
                        <a href="#" style="color: #ca6666;text-decoration:none;">Android Development</a>
                        <a href="#" style="color: #ca6666;text-decoration:none;">iOS Development</a>
                        <a href="#" style="color: #ca6666;text-decoration:none;">Hybrid App Development</a>


                      </div>
                      <div class="testcolumn" style="width:24%;">
                        <h3 style="font-size: 18px; color: #ca0000;font-family: FontAwesome;font-weight: normal;">  E-COMMERCE</h3>
                         <a href="#" style="color: #ca6666;text-decoration:none;">Custom ecommerce development</a>
                         <a href="#" style="color: #ca6666;text-decoration:none;">Magento Development</a>
                         <a href="#" style="color: #ca6666;text-decoration:none;">Open cart Development</a>
                       

                       </div>
                       <div class="testcolumn" style="width:24%;">
                        <h3 style="font-size: 18px; color: #ca0000;font-family: FontAwesome;font-weight: normal;">CONTENT MANAGEMENT</h3>
                         <a href="#" style="color: #ca6666;text-decoration:none;">Custom CMS Solutions</a>
                         <a href="#" style="color: #ca6666;text-decoration:none;">WordPress Development</a>
                         <a href="#" style="color: #ca6666;text-decoration:none;">Drupal Development</a>
                         <a href="#" style="color: #ca6666;text-decoration:none;">Joomla Development</a>
                       </div> 
                       </div>
                      
                           
                           
                        </div>
                 </div>
                 </li>
                 
                <li class="dropdown hidden-sm hidden-md hidden-xs" class="nav-item">
                      
                    <a class="nav-link" href="#"  style="color:white;">Virtual Repair&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                <div class="dropdown-content">
                        <a href="pricing.html">Pricing</a>
                        <a href="HowItWorks.html">How it Works</a>
                        <a href="FAQ.html">FAQ</a>

                    </div>
              </li>

            <li class="nav-item">
              <a class="nav-link" href="blogs.html"  style="color:white;">Blog&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Contact.html"  style="color:white;">Contacts&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
              </li>
            <li class="nav-item">
              <a class="nav-link" href="#"  style="color:white;">Login</a>
            </li>

          </ul>
        </div>
      
    </nav>

  
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="images/scan-and-fix-infected-windows-computer.jpg" alt="Los Angeles" style="width:100%;">
        <div class="carousel-caption" style="margin-left:-1000px;margin-top: -1000px;" id="item1"> 
         <a href="index.html"><img src="images/img3.jpeg" height="125px;" width="140px;" style="margin-left:-100px;margin-top: -690px;"></a>
        </div>
      </div>

      </div>
  
    </div>

    <!-- Left and right controls 
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
    -->
  </div>


   
            <div style="width: 100%;">
            <img src="images/scan-and-fix-infected-windows-computer.jpg" height="560px;" alt="Norway" style="width:100%;">
            <div class="centered">
                <h1 style="color:white;"> Three Simple Computer Repair Prices, No Sales Pressure, No Software, No
                 Commitments, No Monthly Fees, No User Accounts, No Credit Card Up Front, 
                 If It’s Not Fixed You’re Not Charged.
                </h1>
            </div>
            </div>
            <div class="container-fluid" style="margin-top:10px;margin-left:40px;">
                <div class="row">
                         <div class="col-sm-4">
                            <div class="col-spaced">
                                <div id="containerIntro" style="background-color: red;padding-left: 25px;padding-top:-20px;">
                                        <br>
                                        <div style=" margin-top: -20px;">
                                        <h1 style="color: white;font-family: Museo500Regular;padding-top:-20px;">$29.25</h1>
                                        <p style="color: white;font-family: Museo500Regular;">/One Time Charge</p>
                                         <form  action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
                                               <input type="hidden" name="cmd" value="_cart" />
                                               <input type="hidden" name="upload" value="1" />
 											   <input type="hidden" name="business" value="amoldevadhe94-facilitator@gmail.com" />

											   <input type="hidden" name="item_name_1" value="" />
                                               <input type="hidden" name="amount_1" value="29.25"/>
                                               <input type="hidden" name="quantity_1" value="1" />
                                               <input type="hidden" name="return" value="http://localhost:8080/WebAPPLogin/pricing.html" />
                                                <input type="hidden" name="cancel_return" value="http://localhost:8080/WebAPPLogin/success.html">
                                              <input type="submit"  value="Buy" style="padding:8px;background-color:yellow;color:blue;width:70px;"/>
                                               </form> 
                                                
                                        <hr style="border:0.5px solid gray;">
                                       
                                        <h4 style="color: white; font-family: Museo500Regular;"> Quick Help-30 Minutes</h4>
                                       </div>
                                        <br>
                              </div>
                              <div style="background-color: rgb(235, 227, 227);padding-left: 20px;">
                                    <br>
                                    <div style=" margin-top: -20px;">
                                        <div class="row" style="margin-top:10px;margin-left:10px;">
                                   <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                   <h5 style="margin-top:1.5px;margin-left:10px;"> Money Back Guarantee</h5>
                                   </div>
                                   <div class="row" style="margin-top:10px;margin-left:10px;">
                                        <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                        <h5 style="margin-top:1.5px;margin-left:10px;"> Technicians in the USA</h5>
                                  </div>
                                  <div class="row" style="margin-top:10px;margin-left:10px;">
                                                <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                                <h5 style="margin-top:1.5px;margin-left:10px;"> No Monthly Fees</h5>
                                 </div>
                                 <div class="row" style="margin-top:10px;margin-left:10px;">
                                        <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                        <h5 style="margin-top:1.5px;margin-left:10px;">Free Initial Analysis</h5>
                                </div>
                                <div class="row" style="margin-top:10px;margin-left:10px;">
                                        <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                        <h5 style="margin-top:1.5px;margin-left:10px;"> Quote Before we Start</h5>
                               </div>
                               <div class="row" style="margin-top:10px;margin-left:10px;">
                                    <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                    <h5 style="margin-top:1.5px;margin-left:10px;">Pay with Credit Card or Paypal</h5>
                               </div>

                             </div>
                                    <br>
                             </div>
                            </div>
                          </div>
                          <div class="col-sm-4" style="margin-left:-29px;">
                            <div class="col-spaced">
                                <div id="containerIntro1" style="background-color:#3f2ce2;padding-left: 20px;">
                                        <br>
                                        <div style=" margin-top: -20px;">
                                            <h1 style="color: white;font-family: Museo500Regular;padding-top:-20px;">$59.95</h1>
                                            <p style="color: white;font-family: Museo500Regular;">/One Time Charge</p>
                                               <form  action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
                                               <input type="hidden" name="cmd" value="_cart" />
                                               <input type="hidden" name="upload" value="1" />
 											   <input type="hidden" name="business" value="amoldevadhe94-facilitator@gmail.com" />

											   <input type="hidden" name="item_name_1" value="Errors,Tune Ups,Email Issues" />
                                               <input type="hidden" name="amount_1" value="59.95"/>
                                               <input type="hidden" name="quantity_1" value="1" />
                                               <input type="hidden" name="return" value="http://localhost:8080/WebAPPLogin/pricing.html" />
 
                                              <input type="submit"  value="Buy" style="padding:8px;background-color:yellow;color:blue;width:70px;"/>
                                               </form> 
                                                
                                            <hr style="border:0.5px solid gray;">
                                           
                                            <h4 style="color: white; font-family: Museo500Regular;"> Errors, Tune Ups, Email Issues - 60 Minutes</h4>
                                            </div>
                                        <br>
                              </div>
                              <div style="background-color: rgb(235, 227, 227);padding-left: 20px;">
                                    <br>
                                    <div style=" margin-top: -20px;">
                                            <div class="row" style="margin-top:10px;margin-left:10px;">
                                       <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                       <h5 style="margin-top:1.5px;margin-left:10px;"> Money Back Guarantee</h5>
                                       </div>
                                       <div class="row" style="margin-top:10px;margin-left:10px;">
                                            <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                            <h5 style="margin-top:1.5px;margin-left:10px;"> Technicians in the USA</h5>
                                      </div>
                                      <div class="row" style="margin-top:10px;margin-left:10px;">
                                                    <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                                    <h5 style="margin-top:1.5px;margin-left:10px;"> No Monthly Fees</h5>
                                     </div>
                                     <div class="row" style="margin-top:10px;margin-left:10px;">
                                            <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                            <h5 style="margin-top:1.5px;margin-left:10px;">Free Initial Analysis</h5>
                                    </div>
                                    <div class="row" style="margin-top:10px;margin-left:10px;">
                                            <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                            <h5 style="margin-top:1.5px;margin-left:10px;"> Quote Before we Start</h5>
                                   </div>
                                   <div class="row" style="margin-top:10px;margin-left:10px;">
                                        <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                        <h5 style="margin-top:1.5px;margin-left:10px;">Pay with Credit Card or Paypal</h5>
                                   </div>
    
                                 </div>
                                        <br>
                             </div>
                            </div>
                          </div>
                          <div class="col-sm-4" style="margin-left:-29px;">
                            <div class="col-spaced">
                                <div id="containerIntro2" style="background-color: green;padding-left: 20px;">
                                        <br>
                                        <div style=" margin-top: -20px;">
                                            <h1 style="color: white;font-family: Museo500Regular;padding-top:-20px;">$89.95</h1>
                                            <p style="color: white;font-family: Museo500Regular;">/One Time Charge</p>
                                            
                                           
                                            
                                            
                                     
                                             <form  action="${initParam['posturl']}" method="post">
                                               <input type="hidden" name="cmd" value="_cart" />
                                               <input type="hidden" name="upload" value="1" />
 											   <input type="hidden" name="business" value="${initParam['business']}" />

											   <input type="hidden" name="item_name_1" value="Complex Virus Removal" />
                                               <input type="hidden" name="amount_1" value="89.95"/>
                                               <input type="hidden" name="quantity_1" value="1" />
                                               <input type="hidden" name="return" value="${initParam['returnurl']}" />
                                              <input type="submit"  value="Buy" style="padding:8px;background-color:yellow;color:blue;width:70px;"/>
                                               </form> 
                                      
                                              
                                            <hr style="border:0.5px solid gray;">
                                           
                                            <h4 style="color: white; font-family: Museo500Regular;">Complex Virus Removal - Over 90 Mins </h4>
                                        </div>
                                        <br>
                              </div>
                              <div style="background-color: rgb(235, 227, 227);padding-left: 20px;">
                                    <br>
                                    <div style="margin-top: -20px;">
                                       <div class="row" style="margin-top:10px;margin-left:10px;">
                                       <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                       <h5 style="margin-top:1.5px;margin-left:10px;"> Money Back Guarantee</h5>
                                       </div>
                                       <div class="row" style="margin-top:10px;margin-left:10px;">
                                            <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                            <h5 style="margin-top:1.5px;margin-left:10px;"> Technicians in the USA</h5>
                                      </div>
                                      <div class="row" style="margin-top:10px;margin-left:10px;">
                                                    <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                                    <h5 style="margin-top:1.5px;margin-left:10px;"> No Monthly Fees</h5>
                                     </div>
                                     <div class="row" style="margin-top:10px;margin-left:10px;">
                                            <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                            <h5 style="margin-top:1.5px;margin-left:10px;">Free Initial Analysis</h5>
                                    </div>
                                    <div class="row" style="margin-top:10px;margin-left:10px;">
                                            <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                            <h5 style="margin-top:1.5px;margin-left:10px;"> Quote Before we Start</h5>
                                   </div>
                                   <div class="row" style="margin-top:10px;margin-left:10px;">
                                        <span  class="glyphicon glyphicon-ok" style="color:blue;"></span> 
                                        <h5 style="margin-top:1.5px;margin-left:10px;">Pay with Credit Card or Paypal</h5>
                                   </div>
    
                                 </div>
                                        <br>
                             </div>
                            </div>
                          </div>
                          </div>
            </div>
            <div class="container-fluid" style="margin-top:10px;margin-left:44px;margin-right:45px;">
                <div class="row" style="margin-right:50px;">   
                    <div class="col-md-4">
                       <b> <h1>Quick Pc Repair</h1></b>
                       <br>
                      <b>  <p>Our quick PC repair option is for 30 minutes or less. This includes items like a simple error message, a non working printer, or an application problem like a browser issue. Our tech will tell you before any work begins if your problem falls into this category.</p></b>
                    </div>
                    <div class="col-md-4">
                            <b><h1>PC Tune Up or Repair</h1></b>
                            <br>
                            <b><p>This computer repair session is for 60 minutes of help from our experts. This covers items like slow computers, complex error messages, networking problems, email issues,  and other common problems. Again, our tech will let you know if your problem falls into this category before any work begins.</p></b>
                    </div>
                    <div class="col-md-4">
                            <b><h1>Virus Removal & Repair</h1></b>
                            <br>
                           <b> <p>This PC repair session is for anything over 90 minutes and covers virus removal cost and more complex problems. This is our maximum price for computer repair regardless of how long we work on your computer. Again, your tech will tell you if your PC repair falls into this category before any work begins.</p></b>
                    </div>
                    </div>
             </div>       
<br>

<section id="carousel">    				
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
                    <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
                      <!-- Carousel indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="#fade-quote-carousel" data-slide-to="0"></li>
                        <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
                        <li data-target="#fade-quote-carousel" data-slide-to="2" class="active"></li>
                        <li data-target="#fade-quote-carousel" data-slide-to="3"></li>
                        <li data-target="#fade-quote-carousel" data-slide-to="4"></li>
                        
                      </ol>
                      <!-- Carousel items -->
                      <div class="carousel-inner">
           
                        <div class="item">
                            <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                            <h4 style="margin-left:40%;margin-top:40px;">DAN HOLLIER
                                </h4>  
                            <blockquote>
                                <p>I googled all of the remote tech support companies and researched the reviews for each of them. I finally decided to choose Boxaid based on the fact that they are based in the USA. My problem involved network sharing across Windows 7 and Windows 8.1 platforms. Jonathan with Boxaid solved all of my problems quickly and expertly. Originally we were working with two PC’s and the session was going so well, I expanded to include my other 3 PC’s. Another great thing with the service is that you pay nothing up front. The tech evaluates the problem and quotes a price. What a great way to do business!</p>
                            </blockquote>
                        </div>
                        <div class="active item">
                            <div class="profile-circle" style="background-color: rgba(145,169,216,.2);"></div>
                            <h4 style="margin-left:40%;margin-top:40px;">MIKE CLAGGETT</h4>  
                            <blockquote>

                                <p>These guys are the REAL DEAL! Jonathon just spent (Well I won’t tell you how long) logged into my PC and fixed a problem that was well beyond my expertise at a ridiculously small price. I have finally found my go to guys for PC problems that are out of my wheel house. 5 Stars doesn’t even start to tell the story. Compared to any other company I’ve tried, BoxAid has them all beat hands down.…</p>
                            </blockquote>
                        </div>
                        <div class="item">
                            <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                            <h4 style="margin-left:40%;margin-top:40px;">GARY P.</h4>  
                            <blockquote>
                                <p>I used BoxAid.com on Sunday to have my home PC fixed remotely. “Jon” spent over five hours working on it and it still only cost me $89. I was anxious about doing this remotely but this company was really professional. (Unlike one place I contacted that wanted my credit card information before they would even discuss the problem or provide any pricing.  Jon explained that if I had any additional concerns to email him and he would reconnect.  I did have one issue and we set a time for a later date and he fixed that too.  I am very comfortable recommending Boxaid to everyone.</p>
                            </blockquote>
                        </div>
                        <div class="item">
                            <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                           
                            <h4 style="margin-left:40%;margin-top:40px;">DIANE LACOMBE</h4>                            <br>
                            <blockquote>
                                <p>Hi there, I contacted Boxaid because my computer needed a check-up and repairs… So I chose Boxaid because I liked what I read about the way they did business, their prices and their policies. It is said that when something looks too good to be true it probably is…well not with Boxaid it is NOT. I got Jonathan who proceeded to ”diagnose” what was wrong with my laptop (it took hours) but this man NEVER gave up until my computer was purring like a content kitten… He was very polite, understanding and patient with a novice like me answering all my questions. Finally I had to change my browser and he is the only person on this Earth who could make me do that!!!! He is a sweetie and a computer Angel. I TOTALLY recommend him. Thanks Boxaid and Jonathan for making my life a whole lot easier.</p>
                            </blockquote>
                        </div>
                        <div class="item">
                            <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                            <h4 style="margin-left:40%;margin-top:40px;">MICHELLE C.</h4>  
                            <blockquote>
                                <p>
                                        Zack is the best !!!!! He spent hours attempting to fix my corrupted operating system (Vista). While he wasn’t able to salvage the system he was able to save all my files and install Windows 7 on the computer. His patience and understanding dealing with a non-computer literate person was amazing. My computer is running better than ever, all my files are back in place and I’m thrilled. Zack is a credit to your organization and a great guy. I’m recommending him ans your service to everyone I know.</p>
                            </blockquote>
                        </div>
                      </div>
                    </div>
                </div>							
            </div>
        </div>
    </section>
    <div class="container-fluid" style="background-color:rgb(245, 236, 236);">
        <div class="row" style="padding-top:50px;margin-left:15px; ">
            <div class="col-sm-4" style="border:1px solid black;margin-left:0px;">
            <h4  style="font-family: sans-serif;text-align:center;">Looking for a yearly or unlimited plan? Read this to see <a href="#">why we don’t offer unlimited support plans.</a></h4>
            </div>
        
        
                <div class="col-sm-3" style="margin-right:10px;" >
                    <a href="#"><img src="images/AM_mc_vs_dc_ae.jpg"></a>
                </div>
                <div class="col-sm-4" style="border:1px solid black;margin-left:5px;">
                        <h4  style="font-family: sans-serif;">Our MAX charge for tech support (shown above) is $89.95 no matter how complex your PC problem is.</h4>
               </div>  
            </div>
   </div>
   <br>

   <div class="footer">
        <center>  <p style="padding-top: 15px;">© 2018 -  PcRounders LLC - Sitemap </p></center>
  
        </div>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
