<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<html>
<head>
	<title>Login V4</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">



	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>

	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">

	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">

	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">

	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">

	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">

	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">

	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="js/main.js"></script>
	<script src="js/validate.js"></script>
</head>
<body>




	<div class="limiter">
		<div class="container-login100" style="background-image: url('css/bg-01.jpg');">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				
				
               <span id="demo" style="color:red;"></span>
				<form  class="login100-form validate-form" method="Post" >
					<span class="login100-form-title p-b-49">
				    Registration
					</span>

					<div class="wrap-input100" id="firstname">
						
						First Name<input class="input100" type="text" name="firstname" placeholder="Type your Firstname" id="fname" autocomplete="off">
						<span id="firstnameerror" class="text-danger font-weight-bold"></span>
						
					</div>

					<div class="wrap-input100" id="latsname">
						
						Last Name<input class="input100" type="text" name="lastname" placeholder="Type your Lastname" id="lname" autocomplete="off">
						<span id="lastnameerror" class="text-danger font-weight-bold"></span>
						
					</div>
					<div class="wrap-input100" id="email">
						
						Email<input class="input100" type="text" name="email" placeholder="Type your email" id="emailid" autocomplete="off">
						<span id="usernameError" class="text-danger font-weight-bold"></span>
							
					</div>
					<div class="wrap-input100" id="pass">
						
						Password<input class="input100" type="password" name="pass" placeholder="Type your password" id="password" autocomplete="off">
						<span id="passwordError" class="text-danger font-weight-bold"></span>
						
					</div>
					<div class="wrap-input100" id="confirmpass">
						
						Confirm Password<input class="input100" type="password" name="conpass" placeholder="Type your confirm password" id="conpass1" autocomplete="off">
						
						<span id="conpasswordError" class="text-danger font-weight-bold"></span>
					</div>
					
	
				<br>
				<br>
				<input type="button" style="color:blue; background-color: green;"  class="login100-form-btn" name="submit_id" id="btn_id" onclick="return validationregistration();" value="Signup"/>
			
                  <br>
					
				</form>
			
			</div>
		</div>
	</div>
	<div id="dropDownSelect1"></div>
	

</body>
</html>