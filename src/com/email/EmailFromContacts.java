package com.email;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailFromContacts {
	 public static void sendEmail(String host, String port, final String user, final String pass,final String username,String emailid, String sub,String comments1) throws AddressException,MessagingException {
	 
	        // sets SMTP server properties
	        Properties properties = new Properties();
	        properties.put("mail.smtp.host", host);
	        properties.put("mail.smtp.port", port);
	        properties.put("mail.smtp.auth", "true");
	        properties.put("mail.smtp.starttls.enable", "true");
	 
	        // creates a new session with an authenticator
	     
	        Authenticator auth = new Authenticator() {
	            public PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication("info@pcrounders.com", "IPcrounders1!");
	            }
	        };

	        Session session = Session.getInstance(properties,auth);
	 
	        // creates a new e-mail message
	        Message msg = new MimeMessage(session);
	 
	        msg.setFrom(new InternetAddress("info@pcrounders.com"));
	        InternetAddress[] toAddresses = { new InternetAddress(user) };
	        msg.setRecipients(Message.RecipientType.TO, toAddresses);
	        msg.setSubject(sub);
	        msg.setSentDate(new Date());
	       	msg.setText("From Contact Section "+"\n"+"\n"+"Username:"+username+"\n"+"Emailid:"+emailid+"\n"+"Message:"+comments1);
	 
	        // sends the e-mail
	        Transport.send(msg);
	 
	    }
}