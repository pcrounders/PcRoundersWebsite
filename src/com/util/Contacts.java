package com.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.DbOperation;
import com.email.EmailFromContacts;
import com.email.EmailUtility;

/**
 * Servlet implementation class Contacts
 */
public class Contacts extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 private String host;
	   private String port;
	   private String user;
	   private String pass;
	
	public void init() {
		  
		 
	        // reads SMTP server setting from web.xml file
	        ServletContext context = getServletContext();
	        host = context.getInitParameter("host");
	        port = context.getInitParameter("port");
	        user = context.getInitParameter("user");
	        System.out.println("user address:"+user);
	        pass = context.getInitParameter("pass");
	    }
	     
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		String username=request.getParameter("username1");
		String emailid=request.getParameter("emailid1");
		String sub=request.getParameter("sub1");
		String comments1=request.getParameter("comments1");
		System.out.println("value of firstname:"+username);
		System.out.println("value of emailid:"+emailid);
		System.out.println("value of sub:"+sub);
		System.out.println("value of enquiry:"+comments1);
		DbOperation db=new DbOperation();
		String msg1=db.InsertContacts(username, emailid,sub,comments1);
		System.out.println(msg1 +"message in Contacts");
		  String resultMessage = "";
			 
	        try
	        {   
	        	if(msg1=="success"){
	            EmailFromContacts.sendEmail(host, port, user, pass, username, emailid,sub,comments1);
	            resultMessage = "success";
	        	}
	        	else{
	            resultMessage = "failed";
	        	}
	        } catch (Exception ex)
	        {
	            ex.printStackTrace();
	            resultMessage = "failed";
	        }
	        finally 
	        {
	        	/*
	            request.setAttribute("Message", resultMessage);
	            getServletContext().getRequestDispatcher("/Result.jsp").forward(
	                    request, response);
	                    */
	        	response.getWriter().write(resultMessage);
	        }
	    }
		
		
	

}
