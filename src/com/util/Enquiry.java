package com.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.DbOperation;
import com.email.EmailUtility;

/**
 * Servlet implementation class Enquiry
 */
public class Enquiry extends HttpServlet {
	private static final long serialVersionUID = 1L;
	   private String host;
	   private String port;
	   private String user;
	   private String pass;
	
	public void init() {
		  
		 
	        // reads SMTP server setting from web.xml file
	        ServletContext context = getServletContext();
	        host = context.getInitParameter("host");
	        port = context.getInitParameter("port");
	        user = context.getInitParameter("user");
	        System.out.println("user address:"+user);
	        pass = context.getInitParameter("pass");
	    }
	 
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		String username=request.getParameter("username");
		String emailid=request.getParameter("emailid");
		String mob=request.getParameter("mob");
		String sub=request.getParameter("sub");
		String enquiry=request.getParameter("enquiry");
		System.out.println("value of firstname:"+username);
		System.out.println("value of emailid:"+emailid);
		System.out.println("value of mob:"+mob);
		System.out.println("value of sub:"+sub);
		System.out.println("value of enquiry:"+enquiry);
		DbOperation db=new DbOperation();
		String msg1=db.InsertEnquiry(username, emailid,mob,sub,enquiry);
		    
	 
	        String resultMessage = "";
	 
	        try
	        {   
	        	if(msg1=="success"){
	            EmailUtility.sendEmail(host, port, user, pass, emailid, sub,enquiry,username,mob);
	            System.out.println("mail sent succefully");
	            resultMessage = "success";
	        	}
	        	else{
	            resultMessage = "failed";
	        	}
	        } catch (Exception ex)
	        {
	            ex.printStackTrace();
	            System.out.println("mail not sent");
	            resultMessage = "";
	        } finally 
	        {
	        	/*
	            request.setAttribute("Message", resultMessage);
	            getServletContext().getRequestDispatcher("/Result.jsp").forward(
	                    request, response);
	                    */
	        	response.getWriter().write(resultMessage);
	        }
	    }
		
		
	

}
