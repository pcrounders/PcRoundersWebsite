package com.model;

public class User {
String firstname;
String lastname;
String email;
String password;
String confirmpass;
public User(String firstname, String lastname, String email, String password, String confirmpass) {
	super();
	this.firstname = firstname;
	this.lastname = lastname;
	this.email = email;
	this.password = password;
	this.confirmpass = confirmpass;
}
public User(){
	
}
public String getFirstname() {
	return firstname;
}
public void setFirstname(String firstname) {
	this.firstname = firstname;
}
public String getLastname() {
	return lastname;
}
public void setLastname(String lastname) {
	this.lastname = lastname;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getConfirmpass() {
	return confirmpass;
}
public void setConfirmpass(String confirmpass) {
	this.confirmpass = confirmpass;
}
@Override
public String toString() {
	return "User [firstname=" + firstname + ", lastname=" + lastname + ", email=" + email + ", password=" + password
			+ ", confirmpass=" + confirmpass + "]";
}

}
